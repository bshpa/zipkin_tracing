import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

@RestController
@EnableAutoConfiguration
public class Service1 {
	private static final Log log = LogFactory.getLog(Service1.class);
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Autowired
	RestTemplate restTemplate;

    @RequestMapping("/")
    String home() {
    	log.info("Home page");
    	String res = restTemplate.getForObject("http://service2:8070/", String.class);
        return res;
    }
    
    @RequestMapping("/exception")
    String exception() {
    	log.info("Exception method");
    	String res = restTemplate.getForObject("http://service2:8070/exception/", String.class);
    	return res;
    }

    public static void main(String[] args) throws Exception {
    	SpringApplication app = new SpringApplication(Service1.class);
        app.run(args);
    }

}
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@EnableAutoConfiguration
public class Service2 {
	private static final Log log = LogFactory.getLog(Service2.class);
	
	@Bean
	RestTemplate RestTemplate() {
		return new RestTemplate();
	}
	
	@Autowired
	RestTemplate restTemplate;

    @RequestMapping("/")
    String home() {
    	log.info("service2 home page");
    	String res = restTemplate.getForObject("http://service3:8080/", String.class);
    	res += restTemplate.getForObject("http://service4:8090/", String.class);
        return res;
    }
    
    @RequestMapping("exception")
    String exception() {
    	log.info("service 2 exception method");
    	String res = restTemplate.getForObject("http://service3:8080/", String.class);
    	restTemplate.getForObject("http://service4:8090/exception/", String.class);
    	return res;
    }

    public static void main(String[] args) throws Exception {
    	SpringApplication app = new SpringApplication(Service2.class);
        app.run(args);
    }

}
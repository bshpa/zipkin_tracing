import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
public class Service4 {
	private static final Log log = LogFactory.getLog(Service4.class);

    @RequestMapping("/")
    String home() {
    	log.info("serice4 home page");
        return "World!";
    }
    
    @RequestMapping("/exception")
    String exception() throws Exception {
    	log.info("service4 exception method");
    	throw new Exception();
    }

    public static void main(String[] args) throws Exception {
    	SpringApplication app = new SpringApplication(Service4.class);
        app.run(args);
    }

}
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.cloud.sleuth.Tracer;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
public class Service3 {
	private static final Log log = LogFactory.getLog(Service3.class);

    @RequestMapping("/")
    String home() {
    	log.info("serice3 home page");
        return "Hello ";
    }

    public static void main(String[] args) throws Exception {
    	SpringApplication app = new SpringApplication(Service3.class);
        app.run(args);
    }

}